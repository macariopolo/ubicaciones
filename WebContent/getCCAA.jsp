<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>
<%@ page import="edu.uclm.esi.common.server.persistence.*, java.sql.*, org.json.*" %>

<%
response.setHeader("Access-Control-Allow-Origin", "*");
response.setHeader("Access-Control-Allow-Credentials", "true");

String sql="Select id, nombre from Ubicaciones where tipo='Comunidad autónoma' order by nombre";
Connection bd=Broker.get().getDBSelector();
PreparedStatement p=bd.prepareStatement(sql);
JSONArray jsa=new JSONArray();
ResultSet r=p.executeQuery();
while (r.next()) {
	JSONObject jso=new JSONObject();
	jso.put("id", r.getInt(1));
	jso.put("nombre", r.getString(2));
	jsa.put(jso);
}
bd.close();
%>
<%= jsa.toString() %>