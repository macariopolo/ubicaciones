<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>
<%@ page import="edu.uclm.esi.common.server.persistence.*, java.sql.*, org.json.*" %>

<%
response.setHeader("Access-Control-Allow-Origin", "*");
response.setHeader("Access-Control-Allow-Credentials", "true");

String provincia=request.getParameter("provincia");
JSONArray jsa=new JSONArray();

if (provincia!=null && provincia.trim().length()>0) {
	String sql="Select id, nombre from Ubicaciones where tipo='Municipio' and idPadre=? order by nombre";
	Connection bd=Broker.get().getDBSelector();
	PreparedStatement p=bd.prepareStatement(sql);
	p.setInt(1, Integer.parseInt(provincia));
	ResultSet r=p.executeQuery();
	while (r.next()) {
		JSONObject jso=new JSONObject();
		jso.put("id", r.getInt(1));
		jso.put("nombre", r.getString(2));
		jsa.put(jso);
	}
	bd.close();
}
%>
<%= jsa.toString() %>